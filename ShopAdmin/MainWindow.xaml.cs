﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ShopAdmin
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Product> products;
        public MainWindow()
        {
            InitializeComponent();
            products = new ObservableCollection<Product>
            {
                new Product
                {
                    Id=1,
                    Name="Chocolate",
                    Price=1200,
                    MadeIn="Thailand",
                    Date=new DateTime(2017, 10 ,20).Date,
                },

                 new Product
                {
                    Id=1,
                    Name="Chocolate",
                    Price=1200,
                    MadeIn="Thailand",
                    Date=new DateTime(2017, 10 ,20).Date,
                },

                  new Product
                {
                    Id=1,
                    Name="Chocolate",
                    Price=1200,
                    MadeIn="Thailand",
                    Date=new DateTime(2017, 10 ,20).Date,
                },
             };
            productList.ItemsSource = products;
            
        }
    }
}
